\documentclass[oneside,spanish]{amsart}
\usepackage[T1]{fontenc}
\usepackage{float}
\usepackage{amstext}
\usepackage{amsthm}
\usepackage{amssymb}
\PassOptionsToPackage{normalem}{ulem}
\usepackage{ulem}
\usepackage{graphicx, color}

\makeatletter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Some new commands
\newcommand{\noun}[1]{\textsc{#1}}
\floatstyle{ruled}
\newfloat{algorithm}{tbp}{loa}
\providecommand{\algorithmname}{Algoritmo}
\floatname{algorithm}{\protect\algorithmname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Textclass specific LaTeX commands.
\numberwithin{equation}{section}
\numberwithin{figure}{section}
\theoremstyle{definition}
\newtheorem*{defn*}{\protect\definitionname}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% User specified LaTeX commands.
\usepackage{xmpmulti}
\usepackage{algorithm,algpseudocode}

\makeatother

\usepackage{babel}
\addto\shorthandsspanish{\spanishdeactivate{~<>}}

\providecommand{\definitionname}{Definición}

\begin{document}
\title{Taller 4: Problema de la 01-mochila}
\author{Juan Sebastián Barreto Jiménez $^1$ \hspace{1cm} Janet Chen He$^1$}
\date{\today	
\\
$^1$Departamento de Ingeniería de Sistemas, Pontificia Universidad           Javeriana\\Bogotá,  Colombia \\
      \texttt{\{juan\_barreto, j-chen\}@javeriana.edu.co}\\~\\}
\begin{abstract}
En este documento se presenta el desarrollo del taller 4, con programación dinámica, para solucionar el problema de la mochila 01, que consta de maximizar la cantidad de elementos con un peso y un precio que se puede guardar en ella. Teniendo en cuenta que estos no se pueden repetir y la mochila tiene un peso máximo.
\end{abstract}
\maketitle

\part{Análisis y diseño del problema}

\section{Análisis}
Para resolver el problema de la mochila 01 con programación dinámica se tiene como primera instancia pensar en una solución recurrente, para ello se debe analizar los casos a cumplir, es decir, el caso base, que pasa si el peso del elemento es mayor a la mochila y si finalmente elemento ayuda o no a maximizar el resultado.

\subsection{Formulación de la ecuación}
Teniendo lo anterior, se presenta entonces la ecuación: \\ \\
\[
g[i,w]=\begin{Bmatrix}
0 & ;i=0 \\
g(i-1,w) & ;w_{i} > W \\ 
max(g(i-1,w),g(i-1,W-w_{i})+v_{i})  & ;w_{i}\leq W
\end{Bmatrix}
\]

\section{Diseño}

\begin{defn*}
Entradas:
\begin{enumerate}
\item $ O < v_{i} \in \mathbb N , w_{i} \in \mathbb N >$, tupla de dos secuencias que contiene elementos, que pertenece a los naturales. La primera secuencia representa los valores de los elementos y la segunda los pesos de los elementos.
\item $W\_max \in \mathbb N$, valor del peso máximo de la mochila, que pertenece a los naturales.
\item $i \in \mathbb N$, valor de la posición del elemento a evaluar, que pertenece a los naturales.
\end{enumerate}
\end{defn*}
~~~~
\newpage
\begin{defn*}
Salidas: \\
\begin{enumerate}
\item $V\_max \in \mathbb R$, suma total de los precios de la máxima cantidad de elementos a guardar en la mochila sin que se pase de su peso máximo.
\end{enumerate}
\end{defn*}
~~~~~

\part{Algoritmos}

\section{Ejercicio}

\subsection{Algoritmo inocente}

Este algoritmo inocente, consta de presentar una solución recurrente. 

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{knapsack\_01}{$O,W\_max$}
\Require{$O = < v = \{v\_i \in N+\} , w = \{w\_i \in N+\} | 1 <= i <= n >$}
\Require{$W\_max \in N+$}
\State\Return{$O,|O|,W\_max$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Inocente}
\end{algorithm}

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{knapsack\_01Aux}{$O,i,W\_max$}
    \If{$i = 0$}
        \State\Return{$0$}
    \Else
        \If{$w[i] > W$}
            \State\Return{\Call{knapsack\_01Aux}{$O,i-1,W\_max$}}
        \Else
            \State$a\leftarrow$ \Call{knapsack\_01Aux}{$O,i-1,W\_max$}
            \State$b\leftarrow$ \Call{knapsack\_01Aux}{$O,i-1,W\_max - w[i]$} + $v[i]$
            \If{$a < b$}
                \State\Return{$b$}
            \Else
                \State\Return{$a$}
            \EndIf
        \EndIf
    \EndIf
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Inocente}
\end{algorithm}

\newpage

\subsection{Algoritmo Memoización}

Este algoritmo de memoización consta de reemplazar todo los retornos por un tabla que almacena el dato necesario, haciendo que no se tenga que calcular el valor de un elemento ya analizado. En el caso de este ejercicio, se concluye que es una tabla de dos dimensiones y se puede inicializar con valores muy pequeños como un -inf.

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{knapsack\_01}{$O,W\_max$}
\Require{$O = < v = \{v\_i \in N+\} , w = \{w\_i \in N+\} | 1 <= i <= n >$}
\Require{$W\_max \in N+$} \\
\textbf{let} $K \in N+ \leftarrow -inf$
\State\Return{$O,|O|,W\_max,K$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Memoización}
\end{algorithm}

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{knapsack\_01Aux}{$O,i,W\_max,K$}
    \If{$K[i, W\_max] = -inf$}
        \If{$i = 0$}
            \State$K[i, W\_max]\leftarrow 0$
        \EndIf
    \Else
        \If{$w_{i} > W\_max$}
            \State$K[i, W\_max]\leftarrow$ \Call{knapsack\_01Aux}{$O,i-1,W\_max,K$}
        \Else
            \State$a\leftarrow$ \Call{knapsack\_01Aux}{$O,i-1,W\_max,K$}
            \State$b\leftarrow$ \Call{knapsack\_01Aux}{$O,i-1,W\_max - w[i],K$} + $v[i]$
            \If{$a < b$}
                \State$K[i, W\_max]\leftarrow b$
            \Else
                \State$K[i, w]\leftarrow a$
            \EndIf
        \EndIf
    \EndIf
    \State\Return{$K[n, W\_max]$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Memoización}
\end{algorithm}

\newpage

\subsection{Algoritmo Botton-up}

El botton-up permite pasar lo anterior a interactivo, ya que como se podrá evidenciar en la implementación, la solución inocente presenta muchos llamados recurrentes, también para disminuir la complejidad, si se inicializa la tabla de memoización en 0 no toca validar el caso base.

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{knapsack\_01}{$O,W\_max$}
\Require{$O = < v = \{v\_i \in N+\} , w = \{w\_i \in N+\} | 1 <= i <= n >$}
\Require{$W\_max \in N+$} \\
\textbf{let} $K \in N+ \leftarrow 0$

\For{$i\leftarrow1 \hspace{0.2cm} \textbf{to} \hspace{0.2cm} n $}
    \For{$w\leftarrow0 \hspace{0.2cm} \textbf{to} \hspace{0.2cm} W\_max $}
        \If{$w_{i} > w$}
            \State$K[i, w]\leftarrow$ K[$i-1,w$]
        \Else
            \State$a\leftarrow$ K[$i-1,w$]
            \State$b\leftarrow$ K[$i-1,w - w[i]$] + $v[i]$
            \If{$a < b$}
                \State$K[i, w]\leftarrow b$
            \Else
                \State$K[i, w]\leftarrow a$
            \EndIf
        \EndIf
    \EndFor
\EndFor
\State\Return{$K[n, W\_max]$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Botton-up}
\end{algorithm}

\newpage

\subsection{Algoritmo Botton-up con Backtracking}

Finalmente para backtracking, se usa otra tabla adicional que permite guardar los datos necesarios para obtener la solución.

\begin{algorithm}[H]
\begin{algorithmic}[1]

\Procedure{knapsack\_01}{$O,W\_max$}
\Require{$O = < v = \{v\_i \in N+\} , w = \{w\_i \in N+\} | 1 <= i <= n >$}
\Require{$W\_max \in N+$} \\
\textbf{let} $K \in N+ \leftarrow 0$
\textbf{let} $B \in Z \leftarrow -1$

\For{$i\leftarrow1 \hspace{0.2cm} \textbf{to} \hspace{0.2cm} n $}
    \For{$w\leftarrow0 \hspace{0.2cm} \textbf{to} \hspace{0.2cm} W\_max $}
        \If{$w_{i} > w$}
            \State$K[i, w]\leftarrow$ K[$i-1,w$]
            \State$B[i, w]\leftarrow$ i - 1
        \Else
            \State$a\leftarrow$ K[$i-1,w$]
            \State$b\leftarrow$ K[$i-1,w - w[i]$] + $v[i]$
            \If{$a < b$}
                \State$K[i, w]\leftarrow b$
                \State$B[i, w]\leftarrow i$
            \Else
                \State$K[i, w]\leftarrow a$
                \State$B[i, w]\leftarrow i-1$
            \EndIf
        \EndIf
    \EndFor
\EndFor
\State\Return{$K[n, W\_max], B[n, W\_max]$}
\EndProcedure

\end{algorithmic}

\caption{Ejercicio - Botton-up con Backtracking}
\end{algorithm}

\part{Implementación}
Para la implementación se usa la herramienta de QBasic, entorno de desarrollo integrado e intérprete para una variante del lenguaje de programación BASIC basada en el compilador QuickBASIC. (Wikipedia)
En donde, para facilidad solo se realizo la versión de Botton-up con Backtracking.
\textbf{En código se anexa en GitLab.}
\end{document}