# **TALLER 4 - PROGRAMACIÓN DINÁMICA** <br/>
Problema de la 01 Mochila <br/>

## Presentado por :star2:

* Juan Sebastián Barreto Jiménez
* Janet Chen He

## Objetivo
Escribir formalmente e implementar un algoritmo basado en la estrategia "programacion dinámica".

## Descripción
El problema a trabajar en este taller es "resolver el problema de la 01-mochila".

Para esto, usted debe crear un programa en QBasic que:

1. Reciba por línea de comandos:
    - El nombre de un archivo donde este la lista de elementos a guardar en la mochila. Cada línea tiene dos números naturales mayores a 0, a saber el peso y el valor de cada elemento.
    - El peso máximo que soporta la mochila.
2. Informe por pantalla la lista de los elementos finalmente guardados.

## Estructura de Archivos
## Estructura de Archivos
1. taller4Algoritmos.bas -> Contiene la implementación de problema de programación dinámica de la mochila 01
2. dataKnapsack.txt -> Archivo uno de texto que contiene los valores de los objetos, precio y peso.
3. dataKnapsack2.txt -> Archivo dos de texto que contiene los valores de los objetos, precio y peso.
4. tallerCuatro.tex -> Contine el documento en .tex
5. Taller_4__Problema_de_la_01_mochila.pdf -> Contine el documento en .pdf