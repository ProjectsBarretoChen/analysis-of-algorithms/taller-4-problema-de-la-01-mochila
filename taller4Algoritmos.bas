Cls
Dim V$(5)
Dim W$(5)
Dim V(5)
Dim W(5)

Open "dataKnapsack.dat" For Input As #1
Print "El archivo comenzo a leerse"
i = 0
Do While Not EOF(1)
    Line Input #1, SplitString$
    Dim FirstPart$

    Length = Len(SplitString$) 'Get length of string.

    FirstPart$ = ""
    For k = 1 To Length
        'loop through the characters of the string up to the ; character
        Char1$ = Mid$(SplitString$, k, 1) ' get a character from the string
        'Test for valid binary digit.
        If Char1$ = "," Then
            'This is the splitting point so exit the loop
            Exit For
        Else
            FirstPart$ = FirstPart$ + Char1$ ' add 1 character to the end of the first part
        End If
    Next

    V$(i) = FirstPart$
    W$(i) = Mid$(SplitString$, k + 1, Length - k + 1) 'copy from the ; to the end of the string being split

    i = i + 1
Loop
Close #1

Input "W Max = ", W_max$

W_max = Val(W_max$)

For i = 0 To 5
    W(i) = Val(W$(i))
Next i

For i = 0 To 5
    V(i) = Val(V$(i))
Next i

n = UBound(V$, 1)

Dim K(n + 1, W_max + 1)

For i = 0 To n + 1
    For J = 0 To W_max + 1
        K(i, J) = 0
    Next J
Next i

For i = 1 To n
    For w_itr = 0 To W_max
        If w_itr < W(i - 1) Then
            K(i, w_itr) = K(i - 1, w_itr)
        Else
            a = K(i - 1, w_itr)
            b = K(i - 1, w_itr - W(i - 1)) + V(i - 1)
            If a < b Then
                K(i, w_itr) = b
            Else
                K(i, w_itr) = a
            End If
        End If
    Next w_itr
Next i

Print K(n, W_max)

